// Default colors
var brandPrimary  = '#f50000';
var brandSuccess  = '#4dbd74';
var brandInfo     = '#63c2de';
var brandWarning  = '#f8cb00';
var brandDanger   = '#f86c6b';

var grayDark      = '#2a2c36';
var gray          = '#55595c';
var grayLight     = '#818a91';
var grayLighter   = '#d1d4d7';
var grayLightest  = '#f8f9fa';

var UrlServer     = 'http://209.133.199.2/mvrest';
var DBserver      = 'OASYS';

angular
.module('app', [
  'ui.router',
  'oc.lazyLoad',
  'pascalprecht.translate',
  'ncy-angular-breadcrumb',
  'angular-loading-bar',
  'ngSanitize',
  'ngAnimate'
])
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.latencyThreshold = 1;
}])
.run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
  $rootScope.$on('$stateChangeSuccess',function(){
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  });
  $rootScope.$state = $state;
  return $rootScope.$stateParams = $stateParams;
}]);
