// oasys.js
angular
    .module('app')
    .controller('ingresoCtrl', ingresoCtrl)
    .controller('salirCtrl', salirCtrl)
    .controller('menuLateral', menuLateral)
    .controller('OasysBridgeSingleCtrl', OasysBridgeSingleCtrl)
    .controller('OasysBridgeActivityCtrl', OasysBridgeActivityCtrl)
    .controller('OasysBridgeThreadsCtrl', OasysBridgeThreadsCtrl);

var config_con = {
    'url':'http://209.133.199.2/mvrest',
    'project':'OASYS'
}

ingresoCtrl.$inject = ['$scope', '$http', '$location'];
    function ingresoCtrl($scope, $http, $location) {

        $scope.ingresoCLick = function (formIngreso) {

            if(formIngreso == undefined || formIngreso.username == undefined){
                $scope.mensaje_in = 'Enter your user';
                return false;
            }

            if(formIngreso.password == undefined){
                $scope.mensaje_in = 'Enter your password';
                return false;
            }
                $http.post(config_con.url + '/api/' + config_con.project + '/mvRest/security/authenticate?username='+formIngreso.username+'&password='+formIngreso.password+'')
                .then(function successCallback(response) {
                    $scope.token = response.data;
                        localStorage.setItem('currentUser', JSON.stringify({ token: response.data }));
                    $location.path('/dashboard')
                }, function errorCallback(response) {
                    //console.log("Error");
                    $scope.mensaje_in = 'Check your username and password.';
                });
        }
    }


salirCtrl.$inject = ['$scope', '$http', '$location'];
    function salirCtrl($scope, $http, $location) {
    $scope.salirCLick = function () {

        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token; // your token
        localStorage.clear();

        $http.post(config_con.url + '/api/' + config_con.project + '/mvRest/security/logout?token='+token+'')
            .then(function successCallback(response) {
                $scope.token = response.data;
                $location.path('/login')
            }, function errorCallback(response) {
                console.log("Error");
            });
    }
}


menuLateral.$inject = ['$scope', '$http'];
    function menuLateral($scope, $http) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token;
        $http({
            method: 'POST',
            headers:{
                'Authorization': 'Basic '+token,
                'Content-Type': 'application/json'
            },
            data:{
                "IN1":"OASYS_LINK_PARAMS",
                "IN2":"MAINMENU",
                "IN3" :"data",
                "OUT":""
            },
            url: config_con.url + '/api/' + config_con.project + '/MVRest/basic/call/GET_OASBRIDGEDATA/?format=json&account=OASBRIDGE'
        }).then(function successCallback(response) {
            $scope.menu_lateral = response.data.MAINMENU;
        }, function errorCallback(response) {
            console.log(response);
        });
    }

OasysBridgeSingleCtrl.$inject = ['$scope', '$http'];
    function OasysBridgeSingleCtrl($scope, $http) {
        var lista_array = [];
        var array_label = [[]];
        var array_data  = [[]];
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token;
        $http({
            method: 'POST',
            headers:{
                'Authorization': 'Basic '+token,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data:{
                "IN1":"ComoJobs",
                "IN2":"SINGLE",
                "IN3" :"OASBRIDGE_SINGLE_STATS",
                "OUT":""
            },
            url: config_con.url + '/api/' + config_con.project + '/MVRest/basic/call/OASBRIDGE_GETSTATS/?format=json&account=OASBRIDGE'
        }).then(function successCallback(response) {
            $scope.lista    = response.data.SINGLE;
            lista_array     = response.data.SINGLE;
            for (var i = 0; i < lista_array.length; i++) {
                array_label[0].push(lista_array[i]['Filename']);
                array_data[0].push(parseInt(lista_array[i]['Elapsed']));
            }
            $scope.labels = array_label;
            $scope.data = array_data;
        }, function errorCallback(response) {
            console.log(response);
        });
        $scope.series = ['Series A', 'Series B'];
    }

OasysBridgeActivityCtrl.$inject = ['$scope', '$http'];
    function OasysBridgeActivityCtrl($scope, $http) {
        var lista_array = [];
        var array_label = [];
        var array_data  = [];
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token;
        $http({
            method: 'POST',
            headers:{
                'Authorization': 'Basic '+token,
                'Content-Type': 'application/json'
            },
            data:{
                "IN1":"ComoJobs",
                "IN2":"ACTIVITY",
                "IN3" :"OASBRIDGE_ACTIVITY_STATS",
                "OUT":""
            },
            url: config_con.url + '/api/' + config_con.project + '/MVRest/basic/call/OASBRIDGE_GETSTATS/?format=json&account=OASBRIDGE'
        }).then(function successCallback(response) {
            $scope.lista    = response.data.ACTIVITY;
            lista_array     = response.data.ACTIVITY;
            for (var i = 0; i < lista_array.length; i++) {
                array_label.push(lista_array[i]['Processor']);
                array_data.push(lista_array[i]['Recs Read']);
            }
            $scope.labels = array_label;
            $scope.data = array_data;

        }, function errorCallback(response) {
            console.log(response);
        });


    }

OasysBridgeThreadsCtrl.$inject = ['$scope', '$http'];
    function OasysBridgeThreadsCtrl($scope, $http) {
        var lista_array = [];
        var array_label = [];
        var array_data  = [];
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser.token;
        $http({
            method: 'GET',
            headers:{
                'Authorization': 'Basic '+token
            },
            url: config_con.url + '/api/' + config_con.project + '/MVRest/system/QUERY?format=JSON&params=SORT OASYS_BRIDGE_THREADS WITH MSGS > "0" BY PROCESS BY THREAD PROCESS THREAD DATE.IN TIME.IN TIME.OU ELAPSED MSGS RATIO MBYTES BPS&account=OASBRIDGE'
        }).then(function successCallback(response) {
            $scope.lista = response.data;
            lista_array  = response.data;
            for (var i = 0; i < lista_array.length; i++) {
                array_label.push(lista_array[i]['THREAD'] + ' THREAD');
                array_data.push(lista_array[i]['MBYTES']);
            }
            $scope.labels = array_label;
            $scope.data = array_data;
        }, function errorCallback(response) {
            console.log(response);
        });
    }
